﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using ScreenManagerDemo;
using Microsoft.Xna.Framework.Content;

namespace ScreenManagerDemo
{
    class GameScreen : Screen
    {
        protected Texture2D texture;
        protected Rectangle destinationRectagle;

        public GameScreen(GraphicsDevice device,
           ContentManager content) 
            :base(device,content,"gameScreen")
        {
            destinationRectagle =
                new Rectangle(0, 0,
                _device.Viewport.Width,
                _device.Viewport.Height);
        }

        public override bool Init()
        {
            texture = _content.Load<Texture2D>("graphics\\endMenu");
            return base.Init();
        }

        public override void Shutdown()
        {
            base.Shutdown();
        }

        public override void Draw(GameTime gameTime)
        {
            _device.Clear(Color.Brown);
            _spriteBatch.Begin();
            _spriteBatch.Draw(texture, destinationRectagle, Color.White);
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            // Check if n is pressed and go to screen2
            if (Keyboard.GetState().IsKeyDown(Keys.N))
            {
                SCREEN_MANAGER.goto_screen("menuScreen");
            }
            base.Update(gameTime);
        }
    }
}